#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>


#define THREADS_PER_BLOCK 8
#define ARR_SIZE 64


__global__ void  my_add_kernel(int *a, int *b, int*c, int n)

{

	int id = threadIdx.x + blockIdx.x * blockDim.x;

	// check if the id falls within the size
	if(id < n)
		c[id] = a[id] + b[id];

	printf("id=%d a=%d b=%d c=%d,\n",id, a[id], b[id], c[id]);
}



int main()
{
	int *a, *b, *c;// host copies of a, b, c

	int *d_a, *d_b, *d_c;// device copies of a, b, c
	
	int n = ARR_SIZE;
	
	int size = sizeof(int)*n;


	// Allocate space for device copies of a, b, c
	cudaMalloc((void**)&d_a, size);
	cudaMalloc((void**)&d_b, size);
	cudaMalloc((void**)&d_c, size);
	
	a = (int*) malloc(size);
	b = (int*) malloc(size);
	c = (int*) malloc(size);

	for(int i=0;i<n;i++)
	{
		a[i]=i;
		b[i]=i*2;
	}


	cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);


	int threads = THREADS_PER_BLOCK;
	int blocks = ( n + threads -1)/threads;

	my_add_kernel<<<blocks, threads>>>(d_a, d_b, d_c, n);

	
	cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);

	printf("\n Done");

	for(int i=0;i<n;i++)
		printf("%d+%d=%d\n",a[i], b[i], c[i]);

	free(a);
	free(b);
	free(c);
	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);
	return 0;
}


