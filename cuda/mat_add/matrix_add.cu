#include<stdio.h>

#include<cuda.h>
#include<cuda_runtime.h>
#include<driver_functions.h>


#define BLOCK_SIZE 32


// matrix add function
__global__ void mat_add(int *a, int* b, int *c, int R, int C)

{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int j = threadIdx.y + blockIdx.y * blockDim.y;

	int thread_id = i*C + j;
	if(i < R && j < C)
	{
		c[thread_id]= a[thread_id] + b[thread_id];
		printf("a=%d, b=%d c=%d \n",a[thread_id],b[thread_id], c[thread_id]);
	}
	

}


int main()
{
	int R = 128;
	int C = 128;

	int row_size = sizeof(int)*C;

	// int **a = (int**) malloc(sizeof(int*)*R);
	// int **b = (int**) malloc(sizeof(int*)*R);
	// int **c = (int**) malloc(sizeof(int*)*R);
	// int **temp = (int**) malloc(sizeof(int*)*R);

	int *a = (int*) malloc(sizeof(int*)*R*C);
	int *b = (int*) malloc(sizeof(int*)*R*C);
	int *c = (int*) malloc(sizeof(int*)*R*C);
	int *temp = (int*) malloc(sizeof(int*)*R*C);



	printf("allocated number of columns\n");
	// allocation for each row
	// for(int i =0;i<R;i++)
	// {
	// 	a[i] = (int*)malloc(sizeof(int)*C);
	// 	b[i] = (int*)malloc(sizeof(int)*C);
	// 	c[i] = (int*)malloc(sizeof(int)*C);
	// 	temp[i] = (int*)malloc(sizeof(int)*C);

	// }

	printf("allocated number of rowss\n");
	int *d_a;
	int *d_b;
	int *d_c;

	cudaMalloc((void**)&d_a,row_size*R);
	cudaMalloc((void**)&d_b,row_size*R);
	cudaMalloc((void**)&d_c,row_size*R);

	// intialising matrices

	printf("allocated device vars\n");
	// for(int i =0 ;i<R;i++)
	// 	for(int j=0;j<C;j++)
	// 	{
	// 		a[i][j]= i+j;
	// 		b[i][j]= i-j;
	// 		temp[i][j] = a[i][j] + b[i][j];
	// 	}
	for(int i =0 ;i<R;i++)
		for(int j=0;j<C;j++)
		{
			a[i*C+j]= i+j;
			b[i*C+j]= i-j;
			temp[i*C+j] = a[i*C+j] + b[i*C+j];
		}
	

		
	printf("initialized mats\n");
	
	cudaMemcpy(d_a, a, row_size*R, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, b, row_size*R, cudaMemcpyHostToDevice);
	
	
	
	dim3 grid(1,1,1); // grid = number of blocks per dimension
	dim3 threads_per_block(BLOCK_SIZE, BLOCK_SIZE, 1);

	
	grid.x = ((R/BLOCK_SIZE) + ((R%BLOCK_SIZE)==0?0:1));
	grid.y = ((C/BLOCK_SIZE) + ((C%BLOCK_SIZE)==0?0:1));
	

	printf("calling kernel\n");
	mat_add<<<grid, threads_per_block>>>(d_a, d_b, d_c, R, C);
	printf("done calling kernel\n");	
	
	cudaMemcpy(c, d_c, row_size*R, cudaMemcpyDeviceToHost);

	cudaDeviceSynchronize();
	printf("aquired calling kernel\n");

	int flag = 0;
	for(int i =0;i<R;i++)
		for(int j=0;j<C;j++)
			if(c[i*C + j] != temp[i*C + j]) // we change this to normal way gives core dump???
			{
			
				//printf("a=%d, b=%d c=%d temp=%d\n",a[i][j],b[i][j], c[i][j],temp[i][j]);
				flag = 1;
				break;
			}
			if(flag)
	printf("Not Done!\n");
	
	else
	printf("Done");

	free(a);
	free(b);
	free(c);
	free(temp);

	cudaFree(d_a);
	cudaFree(d_b);
	cudaFree(d_c);


	return 0;

}
