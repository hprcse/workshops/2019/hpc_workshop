#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <driver_functions.h>


#define THREADS_PER_BLOCK 8
#define ARR_SIZE 64


__global__ void for_single()
{
	// equivalent to one loop
	int i =  threadIdx.x + (blockIdx.x * blockDim.x);

	printf("GPU i=%d\n",i);
}

// 2 into 2 loop
__global__ void for_double()
{
	// equivalent to 2 loops
	int i =  threadIdx.x + (blockIdx.x * blockDim.x);
	int j =  threadIdx.y + (blockIdx.y * blockDim.y);

	printf("GPU i=%d j=%d\n", i, j);
}

// 3 into 3 loop
__global__ void for_tripple()
{
	// equivalent to 3 loops
	int i =  threadIdx.x + (blockIdx.x * blockDim.x);
	int j =  threadIdx.y + (blockIdx.y * blockDim.y);
	int k =  threadIdx.z + (blockIdx.z * blockDim.z);

	printf("GPU i=%d j=%d  k=%d\n", i, j, k);
}



int main()
{
	int i, j,k;

	int num_threads = 4;

	dim3 grid(1,1,1);
	dim3 block(num_threads, num_threads,num_threads);

	printf("\nSingle Loop CPU\n");

	for(i =0 ;i< num_threads;i++)
		printf("CPU i=%d\n",i);

	printf("\nSingle Loop GPU\n");

	

	for_single<<<1,4>>>();

	cudaDeviceSynchronize();
	printf("\nDouble Loop CPU\n");

	for(i =0 ;i< num_threads;i++)
		for(j =0 ;j< num_threads;j++)
			printf("CPU i=%d j=%d\n",i, j);

	printf("\nDouble Loop GPU\n");

	dim3 grid_2d(1,1,1);
	dim3 block_2d(4,4,1);
	for_double<<<grid_2d,block_2d>>>();
	cudaDeviceSynchronize();

	printf("\nTripple Loop CPU\n");

	for(i =0 ;i< num_threads;i++)
		for(j =0 ;j< num_threads;j++)
			for(k =0 ;k< num_threads;k++)
				printf("CPU i=%d j=%d k=%d\n", i, j, k);
	
	printf("\nTripple Loop GPU\n");

	for_tripple<<<grid,block>>>();
	cudaDeviceSynchronize();





	return 0;
}
